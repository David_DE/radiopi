#!/bin/bash

#Generate playlist from folder
ls /media/usb/ > /var/lib/mpd/playlists/mp3.m3u

sleep 2

#update mpc, clear the current playlist and load the radio.m3u playlist 
mpc update
mpc clear
mpc load radio

sleep 2

#Run the main python script to detect input etc
sudo python /home/pi/projects/RadioPi/radiopi.py &
