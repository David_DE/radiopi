#!/bin/bash

#Clears the current playlist, sets random to off and loads the radio playlist
mpc update
mpc clear
mpc random off
mpc load radio