#RadioPi - Work in progress

The goal for this project is to create an internet radio using a raspberry pi, mpd, mpc, python and gpio for user input.


##Components

###Software
* mpd - Music Player Daemon: For playing the music on the pi. 
* mpc - A minimalist command line interface to MPD.
* Python - The main script for handeling the gpio in and outputs and the display
* RPi.GPIO - A python libary to read from and write to the gpio's
* Shell script - Will be started after booting; Creates a playlist for the files on an usb stick; Will start the python script


###Hardware
* Raspberry Pi
* HD44780 (4x20 Display)
* Lots of wires, Buttons and more


##Setup

A short description of what needs to be done on the software side

1. Getting MPD with MPC running  
    * ...
    * ...
2. ...
3. ...


##ToDo & Ideas
* Improve displayed text (scrolling text?) and timely updates
* When switching back from Mp3 to Radio, play last played radio stream (and vice versa)
* If in Mp3 Mode: Switch to Radio for News and back (e.g. every hour at XX:59)
* Shutdown pi completly after pressing quit button
* Autostart start.sh shell script
* In Mp3 Mode: Button to replay the currently running song once (or again, if pressed again)
* Be able to change the volume?
* Fixed volumes for each radio stream to make sure the volume levels are similar

##Credits

Will be added, when the project is done
