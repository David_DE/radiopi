#!/usr/bin/python
import time
import RPi.GPIO as GPIO
from subprocess import call
import subprocess, shlex

# Zuordnung der GPIO Pins (ggf. anpassen)
DISPLAY_RS = 7
DISPLAY_E  = 8
DISPLAY_DATA4 = 25 
DISPLAY_DATA5 = 24
DISPLAY_DATA6 = 23
DISPLAY_DATA7 = 18

GPIO_LCD_STATUS = 21
GPIO_BUTTON_WEITER = 17
GPIO_BUTTON_RADIOMP3 = 22
GPIO_BUTTON_PlayPause = 10
GPIO_BUTTON_STOP = 9
GPIO_BUTTON_QUIT = 11

DISPLAY_WIDTH = 20 	# Zeichen je Zeile
DISPLAY_LINE_1 = 0x80 	# Adresse der ersten Display Zeile
DISPLAY_LINE_2 = 0xC0 	# Adresse der zweiten Display Zeile
DISPLAY_LINE_3 = 0x94   # LCD RAM address for the 3rd line
DISPLAY_LINE_4 = 0xD4   # LCD RAM address for the 4th line 
DISPLAY_CHR = True
DISPLAY_CMD = False
E_PULSE = 0.00005
E_DELAY = 0.00005


item = 1
running = 1 
play = 1
radio = 1

sender = ["FFH", "HR3", "HR Info"]

def main():
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(DISPLAY_E, GPIO.OUT)
	GPIO.setup(DISPLAY_RS, GPIO.OUT)
	GPIO.setup(DISPLAY_DATA4, GPIO.OUT)
	GPIO.setup(DISPLAY_DATA5, GPIO.OUT)
	GPIO.setup(DISPLAY_DATA6, GPIO.OUT)
	GPIO.setup(DISPLAY_DATA7, GPIO.OUT)
	
	GPIO.setup(GPIO_BUTTON_WEITER, GPIO.IN)
	GPIO.setup(GPIO_BUTTON_RADIOMP3, GPIO.IN)
	GPIO.setup(GPIO_BUTTON_PlayPause, GPIO.IN)
	GPIO.setup(GPIO_BUTTON_STOP, GPIO.IN)
	GPIO.setup(GPIO_BUTTON_QUIT, GPIO.IN)
	
	GPIO.setup(GPIO_LCD_STATUS, GPIO.OUT)

	display_init()
	printRadioToDisplay("RadioPi", "Internet Radio + MP3", None, "(c) David Frost", 2, 2, 1, 3)
	
	radio_init()

	time.sleep(5)
	
	while (running):
		input_btn_weiter = GPIO.input(GPIO_BUTTON_WEITER)
		input_btn_radioMp3 = GPIO.input(GPIO_BUTTON_RADIOMP3)
		input_btn_playPause = GPIO.input(GPIO_BUTTON_PlayPause)
		input_btn_stop = GPIO.input(GPIO_BUTTON_STOP)
		input_btn_quit = GPIO.input(GPIO_BUTTON_QUIT)	
		
		if input_btn_weiter == 1:
			GPIO.output(GPIO_LCD_STATUS,GPIO.HIGH)
			global radio
			
			if radio==1:
				global item
				item += 1
				item = item % 4
				if item==0:
					item = 1

				nextRadio(item)
			else:
				args = shlex.split("mpc next")
				proc = subprocess.Popen(args, stdout=subprocess.PIPE)
				time.sleep(1)
				mp3Status()

   			time.sleep(1)

		elif input_btn_radioMp3 == 1:
			GPIO.output(GPIO_LCD_STATUS,GPIO.HIGH)
			print "----------------RADIO/MP3!----------------"
			
			switchRadioMp3()

			time.sleep(1)
			
		elif input_btn_playPause == 1:
			GPIO.output(GPIO_LCD_STATUS,GPIO.HIGH)

			togglePlayPause()
			
			time.sleep(1)	
		
		elif input_btn_stop == 1:
			GPIO.output(GPIO_LCD_STATUS,GPIO.HIGH)

			stopRadio()
			
			time.sleep(1)	

		elif input_btn_quit == 1:
			quitRadio()
			
		else:
			GPIO.output(GPIO_LCD_STATUS,GPIO.LOW)

		time.sleep(0.15)
	
	GPIO.cleanup()

	
def togglePlayPause():
	print "----------------Play/Pause----------------"
	args = shlex.split("mpc toggle")
	proc = subprocess.Popen(args, stdout=subprocess.PIPE)
	#output = proc.stdout.read()	
	
	global play
	
	if play==1:
		printRadioToDisplay(None, "Status: Pause", None, None, 1, 1, 1, 1)
		play=0
	else:
		printRadioToDisplay(None, "Status: Play", None, None, 1, 1, 1, 1)
		play=1
	

	
	#Stop RadioPi
def stopRadio():
	print "----------------Stop----------------"
	args = shlex.split("mpc stop")
	proc = subprocess.Popen(args, stdout=subprocess.PIPE)
	
	global play
	play=0
	
	printRadioToDisplay(None, "Status: Stop", None, None, 1, 1, 1, 1)

	
	#Toggle between Radio and MP3 mode 
def switchRadioMp3():
	global radio
	global play
	
	play = 1

	if radio==0:
		print "----------------Radio----------------"
		args = shlex.split("bash /home/pi/projects/RadioPi/toRadio.sh")
		proc = subprocess.Popen(args, stdout=subprocess.PIPE)
		global item
		#item = 1
		time.sleep(1)
		nextRadio(item)
		radio = 1
	else:
		print "----------------MP3----------------"
		args = shlex.split(" bash /home/pi/projects/RadioPi/toMp3.sh")
		proc = subprocess.Popen(args, stdout=subprocess.PIPE)
		time.sleep(1)
		mp3Status()
		radio = 0


def mp3Status():
	args = shlex.split("mpc")
	proc = subprocess.Popen(args, stdout=subprocess.PIPE)
	output = proc.stdout.read()		
	lines = output.split('\n')
	printRadioToDisplay(lines[0], "Status: Play", " ", " ", 1, 1, 1, 1) #[:-4]
			
#Pause Playing, Shutdown Pi and close Python script
def quitRadio():
	GPIO.output(GPIO_LCD_STATUS,GPIO.HIGH)
	printRadioToDisplay("Achtung!", "Status: Beenden?", "Button 2s lang", "gedrueckt halten!", 2, 1, 1, 1)
	time.sleep(2)	
	input_btn_quit = GPIO.input(GPIO_BUTTON_QUIT)	
	
	if input_btn_quit==1:
		print "----------------Beenden!----------------"
		global running
		running = 0
		stopRadio()
		printRadioToDisplay(" ", "Raspberry Pi wird ", "herunter gefahren!", " ", 1, 2, 2, 1)
		args = shlex.split("sudo shutdown now")
		proc = subprocess.Popen(args, stdout=subprocess.PIPE)
	
	else: 
		global radio
		if radio==1:
			global sender
			global item
			printRadioToDisplay("Sender: " + sender[item-1], "Status: Play", " ", " ", 1, 1, 1, 1)
			
		else:
			mp3Status()
		

def nextRadio(nr):
	args = shlex.split("mpc play " + str(nr))
	proc = subprocess.Popen(args, stdout=subprocess.PIPE)
	output = proc.stdout.read()
	print output			
	lines = output.split('\n')

	global sender
	printRadioToDisplay("Sender: " + sender[nr-1], "Status: Play", None, None, 1, 1, 1, 1)	
	
#Schreibt eine oder mehrere Zeilen auf das Display
def printRadioToDisplay(z1, z2, z3, z4, s1, s2, s3, s4):

	if not (z1 is None):
		lcd_byte(DISPLAY_LINE_1, DISPLAY_CMD)
		lcd_string(z1, s1)
		
	if not (z2 is None):
		lcd_byte(DISPLAY_LINE_2, DISPLAY_CMD)
		lcd_string(z2, s2)
		
	if not (z3 is None):
		lcd_byte(DISPLAY_LINE_3, DISPLAY_CMD)
		lcd_string(z3, s3)
		
	if not (z4 is None):
		lcd_byte(DISPLAY_LINE_4, DISPLAY_CMD)
		lcd_string(z4, s4)
	

def radio_init():
	global item
	nextRadio(item)
	
	
def display_init():
	lcd_byte(0x33,DISPLAY_CMD)
	lcd_byte(0x32,DISPLAY_CMD)
	lcd_byte(0x28,DISPLAY_CMD)
	lcd_byte(0x0C,DISPLAY_CMD)  
	lcd_byte(0x06,DISPLAY_CMD)
	lcd_byte(0x01,DISPLAY_CMD)  

def lcd_string(message, style):
	if style==1:
		message = message.ljust(DISPLAY_WIDTH," ")  
	elif style==2:
		message = message.center(DISPLAY_WIDTH," ")
	elif style==3:
		message = message.rjust(DISPLAY_WIDTH," ")
	
	for i in range(DISPLAY_WIDTH):
		lcd_byte(ord(message[i]),DISPLAY_CHR)

def lcd_byte(bits, mode):
	GPIO.output(DISPLAY_RS, mode)
	GPIO.output(DISPLAY_DATA4, False)
	GPIO.output(DISPLAY_DATA5, False)
	GPIO.output(DISPLAY_DATA6, False)
	GPIO.output(DISPLAY_DATA7, False)
	
	if bits&0x10==0x10:
	  GPIO.output(DISPLAY_DATA4, True)
	if bits&0x20==0x20:
	  GPIO.output(DISPLAY_DATA5, True)
	if bits&0x40==0x40:
	  GPIO.output(DISPLAY_DATA6, True)
	if bits&0x80==0x80:
	  GPIO.output(DISPLAY_DATA7, True)
	  
	time.sleep(E_DELAY)    
	GPIO.output(DISPLAY_E, True)  
	time.sleep(E_PULSE)
	GPIO.output(DISPLAY_E, False)  
	time.sleep(E_DELAY)      
	
	GPIO.output(DISPLAY_DATA4, False)
	GPIO.output(DISPLAY_DATA5, False)
	GPIO.output(DISPLAY_DATA6, False)
	GPIO.output(DISPLAY_DATA7, False)
	
	if bits&0x01==0x01:
		GPIO.output(DISPLAY_DATA4, True)
	if bits&0x02==0x02:
		GPIO.output(DISPLAY_DATA5, True)
	if bits&0x04==0x04:
		GPIO.output(DISPLAY_DATA6, True)
	if bits&0x08==0x08:
		GPIO.output(DISPLAY_DATA7, True)
	  
	time.sleep(E_DELAY)    
	GPIO.output(DISPLAY_E, True)  
	time.sleep(E_PULSE)
	GPIO.output(DISPLAY_E, False)  
	time.sleep(E_DELAY)   

if __name__ == '__main__':
	main()
